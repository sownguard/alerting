address = 'http://grafana host here/api/annotations'
raw_response_file = 'raw_response.json'

# valid period of alerts in minutes
filter_alerts_min = 5
# the number of blocks from which 
# beeper executes
critical_blocks = 50
beeper_file = '02600.mp3'