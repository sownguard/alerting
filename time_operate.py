import datetime

## This function gets the UNIX timestamp as an argument and gives the difference
## between the current time and the UNIX timestamp in minutes
def timeDelta(unixTime):
	current_time = datetime.datetime.now()
	converted_unix_time = datetime.datetime.fromtimestamp(round(unixTime / 1000))
	time_delta_mins = (current_time - converted_unix_time).total_seconds() / 60
	return time_delta_mins



# fast converter from unix time to normal time
# time = 1602027782051
#converted = datetime.datetime.fromtimestamp(round(time / 1000))

