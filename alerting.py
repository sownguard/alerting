from filter_alerts import Filter_Alerts 
from collections import OrderedDict
import time
import datetime
from beeper import Beeper
from config import *


class Phone_Call():

	def __init__(self):
		sorted_alerts = self.getSort()
		try:
			self.assumption(sorted_alerts[0], critical_blocks)
			self.assumption(sorted_alerts[1], critical_blocks)
			pass
		except Exception as exception:
			print(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) +\
			 '  There are no alerts yet\n')

	def getSort(self):

		# setting up the class instance of Filter_Alerts
		# variable alerts is a list of dicts 
		# with filtered alerts by type and time

		fltr = Filter_Alerts()
		alerts = fltr.filterAlerts(filter_alerts_min)
		segments = []
		blocks = []


		# dividing all alerts to separate lists
		for i in range(0, len(alerts)):
			if alerts[i]['id'] == 1:
				alerts[i]['type'] = 'segments'
				segments.append(alerts[i])
			elif alerts[i]['id'] == 10 or alerts[i]['id'] == 11:
				alerts[i]['type'] = 'blocks'
				blocks.append(alerts[i])

		# sort lists of alerts by time
		sorted_blocks = sorted(blocks, key=lambda i: i['time'])
		sorted_segments = sorted(segments, key=lambda i: i['time'])

		# print alerts that in "time filtered period"
		# line with printing the state of alert
		# could be uncommitted 
		for i in range(0, len(sorted_blocks)):
			print(str(sorted_blocks[i]['time']) +
				 #' State: ', sorted_blocks[i]['state'],
				 "   Blocks: " + str(sorted_blocks[i]['value']) )

		for i in range(0, len(sorted_segments)):
			print(str(sorted_segments[i]['time']) +
				 #' State: '+ str(sorted_segments[i]['state']) + 
				 "   Segments down: "+ str(sorted_segments[i]['value']) )

		return sorted_blocks, sorted_segments


	def assumption(self, data, critical = 80):
			
			# if the last alert (the most recent)
			# has the value of blocks / segments - None
			# or the number of blocks is under the 
			# critical number of blocks
			# it prints ...
			# if there are segments down or the
			# number of blocks is higher than critical
			# it prints ALERT and starts beeper

		if data[0]['type'] == 'blocks':
			if data[-1]['value'] == None or data[-1]['value'] < critical:
				print('...\n')
			else:
				print('\n ALERT BLOCKS!!' , data[-1]['value'])
				Beeper()
		elif data[0]['type'] == 'segments':
			if data[-1]['value'] == None:
				print('...\n')
			else:
				print('\n ALERT SEGMENTS DOWN!!' , data[-1]['value'])
				Beeper()

## the script starts every 120 seconds
while True:	
	Phone_Call()
	time.sleep(120)


