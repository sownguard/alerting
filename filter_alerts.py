import json
from time_operate import timeDelta
from web_requests import Get_Alerts
import datetime
from config import *


class Filter_Alerts():

	def filterAlerts(self, valid_period=filter_alerts_min):
		# executes the script of getting the response from Grafana API
		getalert = Get_Alerts()
		alerts = getalert.get_json()

		# an empty array to fill it with valid alerts 
		# depending on valid_period in minutes
		valid_alerts = []

		filtered_alerts = []

		# getting only time-relevant alerts

		# for example - set valid_period as 600 (mins)
		# that equals to 5 hours
		# valid_alerts will be filled only with alerts that
		# occured not earlier than 5 hours ago 
		for each in range(0, len(alerts) - 1):
			if timeDelta(alerts[each]['time']) < valid_period:
				valid_alerts.append(alerts[each])

		# funcion filters valid alerts depending on its type
		# and retrieves only the data we need (time, state, value)
		# as a result we get a list of dictionaries with alerts
		# example:
		# {'id': 11, 'alertName': 'Blocks alert',
		# 'time': datetime.datetime(2020, 10, 9, 8, 57, 10),
		# 'state': 'pending', 'value': 133}

		# the time can be interpreted to readable format with printing
		# print(filterAlerts(valid_alerts)[i]['time'], '\n')

		
		# id's of alerts we want to get
		# 1 - segments down
		# 10, 11 - blocks
		ids = [1, 10, 11]

		for each in range(0, len(valid_alerts)):
			alertId = int(valid_alerts[each]['alertId'])
			# check if alert is "blocks" or 'segments'
			if alertId in ids:
				unixTime = valid_alerts[each]['time']
				time = datetime.datetime.fromtimestamp(round(unixTime / 1000))
				state = valid_alerts[each]['newState']
				alertName = valid_alerts[each]['alertName']
				uniqueAlert = {'id':alertId, 
								'alertName':alertName, 
								'time':time,
								'state':state}
				try:
					value = valid_alerts[each]['data']['evalMatches'][0]['value']
					uniqueAlert['value'] = value
				except Exception as exp:
					if valid_alerts[each]['data'] == {} or valid_alerts[each]['data'] == {'noData':True}:
						uniqueAlert['value'] = None
					else:
						print('ERROR!  ',each ,exp, valid_alerts[each], '\n')

				filtered_alerts.append(uniqueAlert)

		return filtered_alerts

