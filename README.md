<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Grafana_APIalerting_script_0"></a>Grafana API-alerting script</h1>
<h4 class="code-line" data-line-start=1 data-line-end=2 ><a id="This_script_gets_alerts_from_Grafana_API_filter_it_by_alerting_type_1"></a>This script gets alerts from Grafana API, filter it by alerting type,</h4>
<h4 class="code-line" data-line-start=2 data-line-end=3 ><a id="validates_it_by_time_set_in_configpy_2"></a>validates it by time, set in <a href="http://config.py">config.py</a>.</h4>
<h4 class="code-line" data-line-start=3 data-line-end=4 ><a id="The_sound_of_the_beeper_could_be_easily_changed_just_add_the_file_to_the_directory_3"></a>The sound of the beeper could be easily changed, just add the file to the directory</h4>
<h4 class="code-line" data-line-start=4 data-line-end=5 ><a id="and_change_the_value_of_beeper_file_in_configpy_4"></a>and change the value of beeper_file in <a href="http://config.py">config.py</a></h4>
<hr>
<h1 class="code-line" data-line-start=7 data-line-end=8 ><a id="Install_guide_7"></a>Install guide:</h1>
<h5 class="code-line" data-line-start=9 data-line-end=10 ><a id="1_Clone_the_repository_with_9"></a>1. Clone the repository with</h5>
<ul>
<li class="has-line-data" data-line-start="11" data-line-end="13"><code>git clone https://gitlab.com/sownguard/alerting.git</code></li>
</ul>
<h5 class="code-line" data-line-start=13 data-line-end=14 ><a id="2_Install_python3_13"></a>2. Install python3</h5>
<ul>
<li class="has-line-data" data-line-start="14" data-line-end="15"><code>pip install -r requirements.txt</code></li>
</ul>
<h5 class="code-line" data-line-start=15 data-line-end=16 ><a id="3_Edit_files_configpy_and_credspy_15"></a>3. Edit files <a href="http://config.py">config.py</a> and <a href="http://creds.py">creds.py</a></h5>
<h5 class="code-line" data-line-start=16 data-line-end=17 ><a id="4_Execute_the_script_16"></a>4. Execute the script</h5>
<ul>
<li class="has-line-data" data-line-start="17" data-line-end="19"><code>python alerting.py</code></li>
</ul>
<hr>
<h1 class="code-line" data-line-start=21 data-line-end=22 ><a id="Press_Ctrl__Z_to_stop_the_script_if_beeper_started_screaming_21"></a>Press Ctrl + Z to stop the script if beeper started screaming</h1>
<h1 class="code-line" data-line-start=22 data-line-end=23 ><a id="Then_execute_the_script_again_22"></a>Then execute the script again</h1>