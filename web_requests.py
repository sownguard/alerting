import requests
import json
import sys
import os
from config import *
from creds import user, password

class Get_Alerts:
	def __init__(self):
		pass

	def get_json(self):
		# execute request
		try:
			response = requests.get(address, auth=(user, password))
			# transform the text of the response to json
			raw_response = json.dumps(response.json(), sort_keys=True, indent=4)
			text_response = response.text

			# write human-readable json with raw response
			with open(raw_response_file, 'w') as raw_h:
				raw_h.write(raw_response)

			# make raw_response able to operate with
			raw_response = json.loads(raw_response)
			return raw_response

		except Exception as e:
			print("\n-----------------------------------------------\n",
			"Can't make a request, check creds / connection\n",
			"-----------------------------------------------\n")
			exit()
		

		
